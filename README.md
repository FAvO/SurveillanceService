[![Python](https://img.shields.io/badge/using-Python%203-blue.svg?logo=python)](https://www.python.org)
[![License](https://img.shields.io/badge/License-GNU%20General%20Public%20License%20v3.0-green.svg?logo=gnu)](https://www.gnu.org/licenses/gpl-3.0.en.html)
[![Raspberry Pi](https://img.shields.io/badge/designed%20for-Raspberry%20Pi-red.svg?logo=raspberry-pi&color=c51a4a)](https://www.raspberrypi.org)
[![Telegram](https://img.shields.io/badge/possible%20through-Telegram-blue.svg?logo=telegram&color=2ca5e0)](https://telegram.org)

# SurveillanceService

Ein kleines Python3-Skript zur Raumüberwachung via Telegram
mittels eines Raspberry Pi

## Wie nutze ich das Skript?

Um das Skript nutzen zu können, müssen die im nächsten Punkt
benannten Bibliotheken installiert werden, die Konfiguration angepasst werden
und ein Starteintrag im Betriebssystem angelegt werden.
Das könnte mittels eines /etc/init.d/-Eintrags gelöst werden.

## Was brauche ich?

* telepot
* apscheduler

### Installation

    $ pip3 install telepot
    $ pip3 install apscheduler
    $ pip3 install python-vlc
    

# Konfiguration

Das Skript wird über eine JSON-Datei (Standardmäßig: "def_conf.json")  
konfiguriert, wobei die Einstellungen selbsterklärend sein sollten.  


Trotzdem müssen für das Skript Vorbereitungen getroffen werden:  
Damit das Skript funktioniert, muss ein "Bot-Token" generiert und  
in der Einstellungsdatei hinterlegt werden.  
Für weitere Informationen kann die  
[Telgram-Bot Dokumentation](https://core.telegram.org/bots "Link zur Doku")  
zu Rate gezogen werden.  


Danach muss der Eintrag `authorized_users` mit den Telegram-IDs gefüllt werden,  
die danach autorisiert sind den Bot zu nutzen.  
Telegram-IDs sind eindeutige Benutzerkennungen, die in diesem Falle  
genutzt werden, um Missbrauch zu verhindern.  
Um diese ID für einen Account herauszufinden, gibt es ebenfalls einige Bots.  


Einige Einstellungen können "live" im Chat angepasst werden;  
diese Veränderungen werden aber (noch) nicht automatisch gespeichert.  

## Verschiedenes
Im Skript gibt es zu Beginn 2 Listen mit kurzen Texten
`INFO_TEXTE` und `KEIN_GESPRACH`,  
die ohne weiteres verändert werden können.

## Hinweis
Die Funktion /led `an` oder `aus` funktioniert nicht auf allen Modellen,
deshalb ist die Funktion versteckt. 

## Typische Fehler
Probleme mit der Audioausgabe von Sprachnachrichten:
Gegbenenfalls wird unter Raspian das VLC-Binding für Python nicht korrekt
geladen:
Stellen Sie zuerst sicher, dass die Packete `vlc` und `libvlc-dev` installiert
sind.
Sollte der Dienst danach immer noch nicht korrekt arbeiten, können
Sie auch das Binding [hier](http://git.videolan.org/?p=vlc/bindings/python.git;a=tree;f=generated;hb=HEAD "Link zum VLC-Git Repository")
direkt von einem Git-Server herunterladen.
Speichern Sie das Skript einfach zu dem Hauptprogramm; Dann sollte es laufen.
