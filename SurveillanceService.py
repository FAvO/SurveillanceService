#! /usr/bin/python3
# -*- coding: utf-8 -*-
# Autor/Author:     Felix A. v. Oertzen (felixantoniovonoertzen@gmx.de)
# Datum/Date:       07/14/2018
# Zweck:            Raumueberwachung via Telegram
# purpose:          #SurveillanceService

# SurveillanceService - a simple surveillance camera based on a Raspberry Pi
# Copyright (C) 2018  Felix v. Oertzen
# felix@von-oertzen-berlin.de

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


print("Starte SurveillanceService by FAvO...")

# === IMPORTANWEISUNGEN / IMPORT STATEMENTS ===

# Importanweisungen fuer den Telegrambot / Imports for telegram-bot
from apscheduler.schedulers.background import BackgroundScheduler
from telepot.loop import MessageLoop
import telepot
import RPi.GPIO as GPIO

# Import der PiCam-Bib / Import for PiCam
import picamera

# Anderes / Various other imports
from random import randint
from PIL import Image, ImageChops
import io
import time
import json
import PIL
import sys
import vlc
import tempfile
import datetime

# === GLOBALE VARIABLEN / GLOBAL VARIABLES ===

# = Allgemein / General  =
APPNAME = 'SurveillanceService'
APPVERSION = '0.0.6'
PHOTO_JOB_ID = 'photo-job'

# Einige Texte fuer Antworten
INFO_TEXTE = ['Is da jemand in da House?',
 'Wer ist denn da?',
 'Guck mal hier ?',
 'Wer schleicht denn da lang?',
 'Is da wer?',
 'Ne, neeh?!',
 'Das Adlerauge ist wachsam, und sieht wat.',
 'Lässt sich auch noch photographieren... &#129318;',
 'Manno Mensch, du ey du!',
 'So tut mich das...']
KEIN_GESPRACH = [
    'Ich bin gerade nicht so gesprächig.\n' +
        'Probier\'s nachher noch einmal.',
    'Hier ist tote Hose...',
    'Sanft ruht die See (und das Heim)...'
    'Nööö, keine Lust.\nSprich\' mit Jemandem anderen.',
    'Mach du dein Ding; Ich mach hier meins.',
    'Alles cool, Digga!'
]

bot = None
scheduler = BackgroundScheduler()

# Einstellungen des Skriptes, die aus ~def_conf.json geladen werden
# Variables for settings from ~def_conf.json
authorized_users = []
silent = True
diff = 1200
interval = 5
photo_size = 640
is_running = True
is_paused = False
# Kamerainstanz / camera instance
camera = None
camLED = False

# glob. Variable fuer Bildvergleich / global var for image comparation
lastImage = None

# === FUNKTIONEN / FUNCTIONS ===

# Sendet 'message' an alle 'authorized_users'
def broadcastMessage(bot,message, parsemode=None):
    global authorized_users
    for id in authorized_users:
        if (parsemode is None):
            bot.sendMessage(id,message)
        else:
            bot.sendMessage(id,message, parse_mode=parsemode)

# Verarbeiten der eingehenden Nachrichten / Handle incoming messages
def manageMessage(msg):
    global bot, silent, diff, interval, KEIN_GESPRACH, is_paused, scheduler, PHOTO_JOB_ID
    content_type, chat_type, chat_id = telepot.glance(msg)
    
    # Ist der Absender berechtigt? / Validate user
    if not(chat_id in authorized_users):
        return

    ON = ['on', '1', 'an', 'ein']
    OFF = ['off', '0', 'aus', 'still']

    # Telegram auf 'schreibt...' setzen
    bot.sendChatAction(chat_id,"typing")
    if content_type == 'text':
        msg_text = msg['text']
        if msg_text.startswith('/start'):
            bot.sendMessage(chat_id, 
                "*Hallo, ich bin dein Überwachungsbot!* ["+ APPVERSION + "]" +
                "\n\n" +
                "Ich überwache dein Eigenheim oder so etwas ähnliches...\n" +
                "und schicke dir ein paar Fotos, wenn sie dort\n" + 
                "jemand einschleicht?\n\n\n" + 
                "Probiere /hilfe, um eine Liste aller Kommandos anzuzeigen.\n", 
                parse_mode='Markdown')
        elif msg_text.startswith('/intervall'):
            parts = msg_text.split()
            if (len(parts) == 1):
                bot.sendMessage(chat_id, "Das Foto-Intervall " +
                    "liegt zur Zeit bei <code>{}</code> Sekunden."
                        .format(interval),
                    parse_mode='HTML')
            else:
                if (parts[1].isdigit() and (int(parts[1]) > 0)):
                    #Change interval and update scheduler
                    interval = int(parts[1])
                    updateDefaultPhotoJob(interval)
                    broadcastMessage(bot, 'Das Interval wurde nun ' +
                        'auf <code>{}</code> Sekunden festgelegt.'.format(interval),'HTML')
                else:
                    # Variable isnt a digit; returning error statement
                    bot.sendMessage(chat_id,
                        'Invalides Argument;\n' +
                        'Das Intervall muss als Zahl (>0) in Sekunden ' +
                        'angegeben werden.')

        elif msg_text.startswith('/stumm'):
            parts = msg_text.split()
            if (len(parts) == 1):
                bot.sendMessage(chat_id, 'Benachrichtigungen sind ' +
                    '<b>{}</b> gestellt.'
                        .format('laut' if not(silent) else 'stumm'),parse_mode='HTML')
            else:
                if (parts[1].lower() in ON):
                    silent = True
                    broadcastMessage(bot, 'Benachrichtigungen sind ' +
                        '<b>stumm</b> gestellt.','HTML')
                elif (parts[1].lower() in OFF):
                    silent = False
                    broadcastMessage(bot, 'Benachrichtigungen sind ' +
                        '<b>laut</b> gestellt.','HTML')
                else:
                    bot.sendMessage(chat_id, 'Unzulässiges Argument.\n'+
                        'Mögliche Argumente sind\n<code>an ein on 1</code>\n'+
                        'oder\n'+
                        '<code>aus off 0 still</code>.',parse_mode='HTML')
        elif msg_text.startswith('/halten'):
            parts = msg_text.split()
            if len(parts) == 1:
                bot.sendMessage(chat_id, 'Ich wurde pausiert.' if is_paused else 'Ich bin scharf.')
            else:
                if (parts[1].lower() in ON):
                    pauseSurveillance(True)
                    broadcastMessage(bot, "Ich wurde von {} entschärft.".format(str(msg['from']['first_name']) + " " + str(msg['from']['last_name'])))
        
                elif (parts[1].lower() in OFF):
                    pauseSurveillance(False)
                    broadcastMessage(bot, "Ich wurde von {} scharf gemacht.".format(str(msg['from']['first_name']) + " " + str(msg['from']['last_name'])))
                else: 
                    bot.sendMessage(chat_id, 'Unzulässiges Argument.\n'+
                        'Mögliche Argumente sind\n<code>an ein on 1</code>\n'+
                        'oder\n'+
                        '<code>aus off 0 still</code>.',parse_mode='HTML')
        elif msg_text.startswith('/led'):
            parts = msg_text.split()
            if (len(parts) == 1):
                bot.sendMessage(chat_id, 'Die LED ist ' +
                    '<code>{}</code>.'.format("an" if camLED else "aus"), parse_mode='HTML')
            else:
                if (parts[1].lower() in ON):
                    camLED = True
                    GPIO.output(5, True)
                    broadcastMessage(bot, "Die LED wurde von {} ausgeschaltet.".format(str(msg['from']['first_name']) + " " + str(msg['from']['last_name'])))
                elif (parts[1].lower() in OFF):
                    camLED = False
                    GPIO.output(5, False)
                    broadcastMessage(bot, "Die LED wurde von {} aktiviert.".format(str(msg['from']['first_name']) + " " + str(msg['from']['last_name'])))
                else: 
                    bot.sendMessage(chat_id, 'Unzulässiges Argument.\n'+
                        'Mögliche Argumente sind\n<code>an ein on 1</code>\n'+
                        'oder\n'+
                        '<code>aus off 0 still</code>.',parse_mode='HTML')

        elif msg_text.startswith('/empfindlichkeit'):
            parts = msg_text.split()
            if (len(parts) == 1):
                bot.sendMessage(chat_id, 'Die Empfindlichkeit liegt bei ' +
                    '<code>{}</code>.'.format(diff), parse_mode='HTML')
            else:
                if (parts[1].isdigit() and (int(parts[1]) > 0)):
                    diff = int(parts[1])
                    broadcastMessage(bot, 'Die Empfindlichkeit wurde nun ' +
                        'auf <code>{}</code> festgelegt.'.format(diff),'HTML')
                else:
                    # Variable isnt a digit; returning error statement
                    bot.sendMessage(chat_id,
                        'Invalides Argument;\n' +
                        'Die Empfindlichkeit muss als Zahl (>0)' +
                        'angegeben werden.')

        elif msg_text.startswith('/status'):
            bot.sendMessage(chat_id,
                '<b> Statusbericht </b>\n\n' +
                'Benachrichtungen: <code>'+('stumm' if silent else 'aktiv')+'</code> \n' +
                'Empfindlichkeit: <code>' + str(diff) + '</code> \n' +
                'Intervall: <code>' + str(interval) + '</code> sec.\n' +
                'Scharf geschaltet: <code>' + ('Ohja' if not(is_paused) else 'Nein') + '</code>',
                parse_mode='HTML')    
        elif msg_text.startswith('/jetzt'):
            bot.sendMessage(chat_id,
                'Okay, ich mache ein Foto...\n...ist gleich so weit.');
            scheduler.modify_job(PHOTO_JOB_ID,next_run_time=datetime.datetime.now(),args=[True])
        elif msg_text.startswith('/hilfe'):
            bot.sendMessage(chat_id,
             '<b> Hilfe </b>\n' + 
             '/start - startet den Bot\n' +
             '/intervall - gibt das aktuelle Intervall aus\n' +
             '/intervall - <code>[Sekunden]</code> Setzt das Intervall auf die angebene Zeit\n' +
             '/stumm - zeigt, ob die Benachrichtigungen für Bilder stumm gestellt sind\n' +
             '/stumm <code>an</code> oder <code>aus</code> - (de)aktiviert die Benachrichtigungen \n'+
             '/empfindlichkeit - zeigt die aktuelle Empfindlichkeit an\n' +
             '/empfindlichkeit <code>[Zahl]</code> - setzt die Empfindlichkeit auf die übergebene Zahl\n' +
             
             '/halten - zeigt, ob der Bot scharf (gestellt) ist\n' +
             '/halten <code>an</code> oder <code>aus</code> - macht den Bot scharf oder entschärft ihn\n' +
             '/status - zeigt den generellen Status an\n' +
             '/jetzt - macht ein neues Foto\n' + 
             '/hilfe - zeigt diese Kommandoübersicht' , parse_mode='HTML')
        elif msg_text.startswith('/stop'):
            broadcastMessage(bot, "Ich wurde von {} gebeten mich zurückzuziehen.\nIch empfehle mich."
                    .format(str(msg['from']['first_name']) + " " + str(msg['from']['last_name'])))
            stopSurveillance()
        elif msg_text.startswith('/'):
            bot.sendMessage(chat_id,'Dieser Befehl ist mir unbekannt...\n\n' +
                '/hilfe für mehr Informationen.')
        else:
            bot.sendMessage(chat_id,
                KEIN_GESPRACH[randint(0,len(KEIN_GESPRACH)-1)])
    elif content_type == 'voice':
        h, tmp = tempfile.mkstemp(prefix='audio-', suffix='.oga')
        bot.download_file(msg['voice']['file_id'], tmp)
        v = vlc.MediaPlayer(tmp)
        v.play()
        bot.sendMessage(chat_id, 'Deine Nachricht wird wiedergegeben...')
    else:
        bot.sendMessage(chat_id,
            'Dein <code>"{}"</code> ist im digitalen Nirwana gelandet ...'
                .format(str(content_type).title()),
            parse_mode='HTML')

# Wird regelmaessig vom Scheduler aufgerufen, um ein Foto zu schiessen
# Gets trigged by scheduler; takes photo, compares it, and sends image 
# if necassary
def take_photo(take_now):
    global bot, lastImage, authorized_users, diff, silent
    # Schiesst Foto und speichert es im <code>stream</code>
    # Takes image and saves it temporary
    stream = io.BytesIO()
    diffImg = io.BytesIO()
    camera.capture(stream, format='jpeg')
    stream.seek(0)

    # Oeffnet das Bild in Graustufen / Loads image in s/w
    newImage = Image.open(stream).convert('L')
    
    # Bildvergleich / image comparison
    if not(lastImage == None):
        factor = 0.25
        width, height = newImage.size
        # Verkleinert das Bild um factor/ resizes the images with factor
        newImage = newImage.resize((int(width * factor),
                        int (height * factor)),
                    Image.NEAREST)
        imgDiff = ImageChops.difference(newImage,lastImage)
        imgDiff.save(diffImg, 'jpeg')
        
        if (diffImg.tell() > diff or take_now):
            # Broadcast des Bildes an alle Nutzer
            # boradcasts image to all users
            file_id = ""
            for id in authorized_users:
                bot.sendChatAction(id,'upload_photo')
                if (file_id == ""):
                    stream.seek(0)
                    file_id = bot.sendPhoto(chat_id=id,
                                photo=stream,
                                caption=INFO_TEXTE[randint(0,len(INFO_TEXTE)-1)],
                                disable_notification=silent,
                                parse_mode='HTML')['photo'][-1]['file_id']
                else:
                    bot.sendPhoto(chat_id=id,
                                photo=file_id,
                                caption=INFO_TEXTE[randint(0,len(INFO_TEXTE)-1)],
                                disable_notification=silent,
                                parse_mode='HTML')
    lastImage = newImage
    if take_now:
        scheduler.modify_job(PHOTO_JOB_ID,args=[False])

# Aktualisiert das Interval des Photo-Jobs
# Updates the trigger-interval of the scheduler
def updateDefaultPhotoJob(intervall):
    global scheduler, PHOTO_JOB_ID
    scheduler.remove_job(PHOTO_JOB_ID)
    addDefaultPhotoJob(intervall)

# Pausiert/reinitiert die Ueberwachung
# Holds / resumes the surveillance
def pauseSurveillance(shouldPause):
    global scheduler, PHOTO_JOB_ID, is_paused, interval
    if shouldPause == is_paused:
        return None
    if shouldPause:
        is_paused = True
        scheduler.remove_job(PHOTO_JOB_ID)
    else:
        is_paused = False
        addDefaultPhotoJob(interval)

# Stops the Surveilance (stops the scheduler and closes)
# Beendet die Überwachung (hält Schleife an und beendet)
def stopSurveillance():
    global scheduler, PHOTO_JOB_ID, is_running
    scheduler.remove_job(PHOTO_JOB_ID)
    is_running = False

# Startet den Photo-Job / Adds the photo-hob to the scheduler
def addDefaultPhotoJob(intervall):
    global scheduler, PHOTO_JOB_ID
    return scheduler.add_job(take_photo, 'interval',
                            seconds=intervall,
                            id = PHOTO_JOB_ID,
                            replace_existing=True,
                            args=[False])
# Hauptprogramm / main program

def main():
    global photo_size, diff, camera, camLED, authorized_users, bot, silent, interval, is_running
    print("Einstellungen auslesen...")
    # Einstellungen aus ~def_conf.json lesen
    # read settings from ~def_conf.json
    config_filename = "def_conf.json"
    try:
        with open(config_filename, 'r') as config_file:
            config = json.load(config_file)
    except FileNotFoundError:
        print('Error: config file "{}" not found: {}'
            .format(config_filename,config_filename))
        return
    except ValueError as e:
        print('Error: invalid config file "{}": {}'
            .format(config_filename, e))
        return

    token = config.get('bot_token')

    if not token:
        print('Error: `Bot token` is missing')
        return

    # Abfrage der Liste der autorisierten Nutzer
    # Get authorized users list from JSON-config-file
    authorized_users = config.get('authorized_users') 
    
    # Ueberpruefen der Liste der autorisierten Nutzer
    # Check if authorized user list is valid
    if type(authorized_users) is not list or len(authorized_users) == 0:
        print('Error: config file doesn’t contain an `authorized_users` list')
        return
    
    photo_size = config.get('photo_size', photo_size)
    diff = config.get('sensivity', diff)
    silent = config.get('silent',True)
    interval = config.get('interval',5)
    camLED = config.get('camera_led', True)
  
    print("Initieren des Kamera-Moduls..")
    # Initiieren des Kameramoduls / Initiating PiCamera
    camera = picamera.PiCamera()
    camera.saturation = 0
    camera.resolution = (photo_size, int(photo_size*0.75))
    # Vobereiten der Kamera-LED / Preparation for turning of cam-led
    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)
    GPIO.setup(5, GPIO.OUT, initial=camLED) 
    print("Mit telgram.org Verbindung aufnehmen...")
    # Initiieren des Bots / Initiating bot
    bot = telepot.Bot(token)
    # Scheduler starten / start scheduler
    print("Vorbereitungen zur Main-Loop treffen...")
    scheduler.start()
    addDefaultPhotoJob(interval)
    MessageLoop(bot, manageMessage).run_as_thread()
    broadcastMessage(bot,"Dein Überwachungsbot ist aufgewacht...")
    print("Bot ist aufgewacht ...")
    print("... und einsatzbereit.")
    try:
        while is_running:
            time.sleep(1)
    except KeyboardInterrupt:
        print("Mittels Ctrl+C beendet...")
        broadcastMessage(bot, "Dein Überwachungsbot wurde mittels Strg-C beendet.")
        broadcastMessage(bot, "Bot out … [OK]")

if __name__ == '__main__':
    main()


